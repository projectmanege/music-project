package com.mangod.entities;

import lombok.Data;

import java.io.Serializable;

/**
 * signUp
 *
 * @author qs
 * @date 2020/12/20 0020
 **/
@Data
public class SignUp implements Serializable {

    private String phone;
    private String pass;
    private String code;
}
