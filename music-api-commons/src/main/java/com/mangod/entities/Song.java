package com.mangod.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Song
 *
 * @author qs
 * @date 2020/12/14 0014
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Song implements Serializable {

    private Integer id;
    private Integer songId;
    private String songName;
    private String author;
    private String album;
}
