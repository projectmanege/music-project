package com.mangod.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * songWithSongList
 *
 * @author qs
 * @date 2020/12/14 0014
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongWithSongList implements Serializable {

    private Integer id;
    private Integer songId;
    private Integer songListId;
}
