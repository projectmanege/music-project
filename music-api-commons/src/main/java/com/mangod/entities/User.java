package com.mangod.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * User
 *
 * @author qs
 * @date 2020/12/14 0014
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private Integer id;
    private String userId;
    private String password;
    private String userName;
    private String phone;
    private String imgs;
    private String sex;
    private String userSummary;
}
