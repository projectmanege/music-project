package com.mangod.entities;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

/**
 * SongList
 *
 * @author qs
 * @date 2020/12/14 0014
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "songSheet")
public class SongSheet implements Serializable {

    private Integer id;
    private String userId;
    private Integer songListId;
    private String songListName;
    private String songSummary;
    private String imgs;
    private List<Song> songs;
}
