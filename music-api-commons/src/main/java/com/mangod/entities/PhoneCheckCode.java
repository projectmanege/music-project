package com.mangod.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * PhoneCheckCode
 *
 * @author qs
 * @date 2020/12/21 0021
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PhoneCheckCode implements Serializable {
    private String phone;
    private String code;
}
