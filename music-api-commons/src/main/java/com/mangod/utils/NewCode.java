package com.mangod.utils;

/**
 * NewCode
 *
 * @author qs
 * @date 2020/12/19 0019
 **/
public class NewCode {
    private static int newcode;
    public static int getNewcode() {
        return newcode;
    }
    public static void setNewcode(){
        int code = (int)(Math.random()*9999)+100;  //每次调用生成一位四位数的随机数
        if (code < 1000) {
            code += 1000;
        }
        newcode = code;
    }
}
