package com.mangod.utils;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.mangod.lang.Result;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AliyunSmsUtils {

    /**
     * 发送短信验证码
     * @param args
     * @throws ClientException
     */
    public static void main(String[] args) throws ClientException {
        sendSms("15359528109", "1234");
    }
    /** 区域，目前只提供两个固定值，default和cn-hangzhou */
    public static String regionId = "cn-hangzhou";

    /** 短信accessKey */
    public static String accessKey = "LTAI4GCpL4knNh1fd33B52ee";

    /** 短信secret */
    public static String secret = "xQcH1ytWmky1Ngd13fE9heNmZMNEiL";

    /** 域名（固定） */
    public static String domain = "dysmsapi.aliyuncs.com";

    /** 阿里云短信发送版本，官网提供不可随意更改* */
    public static String version = "2017-05-25";

    /** 短信签名 */
    private static String signName = "wmkFirstStud";

    /** 短信模板code */
    private static String templateCode = "SMS_201650354";

    /**
     * 发送短信
     *
     * @param phoneNum 手机号
     * @param code 人名
     * @return
     */
    public static Result sendSms(String phoneNum, String code) throws ClientException {
        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKey, secret);
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain(domain);
        request.setVersion(version);
        // 需要调用方法值，单一发送固定值
        request.setAction("SendSms");
        request.putQueryParameter("PhoneNumbers", phoneNum);
        // 阿里云短信服务中申请的签名
        request.putQueryParameter("SignName", signName);
        // 阿里云短信服务中申请的短信模板代码
        request.putQueryParameter("TemplateCode", templateCode);
        // 短信模板中的变量使用Json方式替换
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", code);
        request.putQueryParameter("TemplateParam", jsonObject.toJSONString());

        CommonResponse response = client.getCommonResponse(request);

        String data = response.getData();

        return Result.succ(response);
    }
}