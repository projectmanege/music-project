package com.mangod.utils;

//import com.mangod.entities.User;
import com.mangod.entities.User;
import io.jsonwebtoken.*;

import java.util.Date;

/**
 * CreateToken
 *
 * @author qs
 * @date 2020/12/15 0015
 **/

public class CreateToken {

    public static String createToken(User user, Date date) {
        //指定签名的时候使用的签名算法，也就是header那部分，jjwt已经将这部分内容封装好了。
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        JwtBuilder builder = Jwts.builder().setHeaderParam("typ","JWT") //设置header
                .setHeaderParam("alg","HS256").setIssuedAt(date)    //设置签发时间
                .setExpiration(new Date(date.getTime() + 1000 * 60 * 60 * 24 * 3))
                .claim("userId",String.valueOf(user.getId())) //设置内容
                .setIssuer("jjq")   //设置签发人
                .signWith(signatureAlgorithm, "musicToken"); //签名，需要算法和key
        String jwt = builder.compact();
        return jwt;
    }
}
