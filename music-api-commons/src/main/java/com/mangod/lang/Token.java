package com.mangod.lang;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Token
 *
 * @author qs
 * @date 2020/12/15 0015
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Token {
    private long tokenId;
    private String userId;
    private String token;
    private int buildTime;
}
