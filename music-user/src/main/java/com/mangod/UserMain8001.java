package com.mangod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * UserMain8001
 *
 * @author qs
 * @date 2020/12/14 0014
 **/

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class UserMain8001 {

    public static void main(String[] args) {
        SpringApplication.run(UserMain8001.class, args);
    }
}
