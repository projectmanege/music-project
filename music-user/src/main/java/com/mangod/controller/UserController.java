package com.mangod.controller;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import com.mangod.entities.PhoneCheckCode;
import com.mangod.entities.SignUp;
import com.mangod.entities.User;
import com.mangod.lang.Result;
import com.mangod.service.IUserService;
import com.mangod.utils.UploadFile;
import com.mangod.utils.AliyunSmsUtils;
import com.mangod.utils.NewCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.concurrent.TimeUnit;

@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    IUserService userService;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    /**
     * 发送手机验证码
     *
     * @param signUp
     * @return
     */
    @PostMapping("/sendCode")
    public Result sendMCode(@RequestBody String signUp) {
        JSONObject JO= (JSONObject) JSONObject.parse(signUp);
        String phone = JO.getString("phone");
        NewCode.setNewcode();
        String code = String.valueOf(NewCode.getNewcode());
        PhoneCheckCode phoneCheckCode = new PhoneCheckCode(phone,code);
        if (userService.selectByPhone(phone) == null) {
            rabbitTemplate.convertAndSend("qs.music", "music.checkCode", phoneCheckCode);
            rabbitTemplate.convertAndSend("qs.music", "music.checkCode", phoneCheckCode);
            rabbitTemplate.convertAndSend("qs.music", "music.checkCode", phoneCheckCode);
            log.info("发送成功");
            return Result.succ(200,"发送成功",null);
        } else {
            return Result.fail(400,"手机号已存在",null);
        }
    }

    @PostMapping("/sendCodeUpdatePass")
    public Result sendCodeUpdatePass(@RequestBody String signUp) {
        JSONObject JO= (JSONObject) JSONObject.parse(signUp);
        String phone = JO.getString("phone");
        NewCode.setNewcode();
        String code = String.valueOf(NewCode.getNewcode());
        PhoneCheckCode phoneCheckCode = new PhoneCheckCode(phone,code);
        rabbitTemplate.convertAndSend("qs.music", "music.checkCode", phoneCheckCode);
        rabbitTemplate.convertAndSend("qs.music", "music.checkCode", phoneCheckCode);
        rabbitTemplate.convertAndSend("qs.music", "music.checkCode", phoneCheckCode);
        log.info("发送成功");
        return Result.succ(200,"发送成功",null);
    }

    /**
     * 监听用户注册发送手机验证码
     *
     * @param phoneCheckCode
     * @return
     */
    @RabbitListener(queues = "music.addUser")
    public void sendCode(PhoneCheckCode phoneCheckCode) {
        log.info(phoneCheckCode.toString());
        try {
            AliyunSmsUtils.sendSms(phoneCheckCode.getPhone(),phoneCheckCode.getCode());
            AliyunSmsUtils.sendSms(phoneCheckCode.getPhone(),phoneCheckCode.getCode());
            Result result = AliyunSmsUtils.sendSms(phoneCheckCode.getPhone(),phoneCheckCode.getCode());
            log.info(String.valueOf(result));
            redisTemplate.opsForValue().set(phoneCheckCode.getPhone(),phoneCheckCode.getCode(), 5, TimeUnit.MINUTES);
            log.info("发送成功");
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

    /**
     * 新增用户
     * @param signUp
     * @return
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody SignUp signUp) {
        User user = new User(null,signUp.getPhone(),signUp.getPass(),signUp.getPhone(),signUp.getPhone(),"..\\favicon.ico","3","这个人太懒了，什么都没有留下。");
        return userService.insert(user, signUp.getCode());
    }

    @GetMapping("/delete")
    public void deleteByPrimaryKey(int id){
        userService.deleteByPrimaryKey(id);
    }

    @GetMapping("/select/{id}")
    public Result selectByPrimaryKey(@PathVariable int id){
        User user = userService.selectByPrimaryKey(id);
        return Result.succ(user);
    }

    @PostMapping("/update")
    public Result updateUser(@RequestBody User user){
        return userService.updateUser(user);
    }

    @PostMapping("/updatePassword")
    public Result updatePassword(@RequestBody SignUp signUp) {
        User user = new User(null,signUp.getPhone(),signUp.getPass(),null,null,null,null,null);
        return userService.updatePassword(user, signUp.getCode());
    }

    @GetMapping("/findByuserId/{userId}")
    public User selectByPrimaryKey(@PathVariable String userId){
        System.out.println("调用了user/select"+userId);
        User user = userService.selectByuserId(userId);
        return user;
    }

    @GetMapping("/selectByuserId/{userId}")
    public Result selectByuserId(@PathVariable String userId){
        System.out.println("调用了user/select"+userId);
        User user = userService.selectByuserId(userId);
        return Result.succ(200, "查询成功", user);
    }

    @PostMapping("/upload")
    public void uploadFile(MultipartFile file){
        UploadFile.uploadFile(file,"C:\\Users\\BeiYe\\Desktop\\imgs\\");
    }

    @GetMapping("/searchUser/{keyword}")
    public Result searchUser(@PathVariable String keyword) {
        List<User> users = userService.searchUser(keyword);
        return Result.succ(users);
    }

}
