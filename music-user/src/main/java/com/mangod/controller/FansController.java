package com.mangod.controller;

import com.mangod.entities.User;
import com.mangod.lang.Result;

import com.mangod.service.FansService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FansController {
    @Autowired
    FansService fansService;

    @GetMapping("/IsFaned/{userId}/{fandId}")
    public  Result IsFaned(@PathVariable String fandId,@PathVariable String userId){
        int num=fansService.IsFaned(fandId,userId);
        return Result.succ(num);
    }

    @GetMapping("/getFansByUserId/{userId}")
    public Result getFansByUserId(@PathVariable String userId){
        System.out.println(userId);
        List<User> users = fansService.selectMyFans(userId);
        return Result.succ(users);
    }
    @GetMapping("/getFanedByuserId/{userId}")
    public Result getFanedByuserId(@PathVariable String userId){
        List<User> users = fansService.selectMyFaned(userId);
        return Result.succ(users);
    }

    /**
     * 取消关注
     * @param fandId 关注的人的id
     * @param userId 当前用户id
     * @return
     */
    @DeleteMapping("/deleteFanedByuserId/{fandId}/{userId}")
    public Result deleteFanedByuserId(@PathVariable String fandId,@PathVariable String userId){
        fansService.deleteFaned(fandId,userId);
        return Result.succ(null);
    }

    /**
     * 关注
     * @param userId 要关注的人
     * @param fansId 当前登录的用户
     * @return
     */
    @PostMapping("/addFans/{userId}/{fansId}")
    public  Result insert(@PathVariable String userId,@PathVariable String fansId){
        fansService.insert(userId,fansId);
        return Result.succ(null);
    }
    @GetMapping("/countMyFans/{userId}")
    public Result countMyFans(@PathVariable String userId){
        int num = fansService.countMyFans(userId);
        return Result.succ(num);
    }
    @GetMapping("/countMyFaned/{userId}")
    public Result countMyFaned(@PathVariable String userId){
        int num = fansService.countMyFaned(userId);
        return Result.succ(num);
    }
}
