package com.mangod.dao;


import com.mangod.entities.Fans;
import com.mangod.entities.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface FansMapper {

    List<User> selectMyFans(String userId);

    List<User> selectMyFaned(String id);

    int deleteFaned(@Param("fanedId") String fanedId, @Param("userId") String userId);

    int insert(@Param("userId") String userId,@Param("fansId") String fansId);

    int countMyFans(String userId);

    int countMyFaned(String userId);

    int IsFaned(@Param("fanedId") String fanedId, @Param("userId") String userId);

}
