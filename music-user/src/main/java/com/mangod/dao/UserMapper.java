package com.mangod.dao;


import com.mangod.entities.SongSheet;
import com.mangod.entities.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {

    @Update("update user set userName = #{userName},imgs = #{imgs},sex = #{sex},userSummary = #{userSummary} where userId = #{userId} ")
    int updateUser(User user);

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    User selectByuserId(String userId);

    User selectByPhone(String phone);


    int updateByPrimaryKeyWithBLOBs(User record);

    int updateByPrimaryKey(User record);

    User findByUsername(String username);

    int updatePassword(User user);

    /**
     * 搜索用户
     * @param keywords
     * @return
     */
    @Select("select distinct * from user where userId like '%${keywords}%' or userName like '%${keywords}%' or phone like '%${keywords}%'")
    List<User> searchUser(String keywords);
}