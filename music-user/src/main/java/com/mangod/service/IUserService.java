package com.mangod.service;

import com.mangod.entities.User;
import com.mangod.lang.Result;

import javax.validation.constraints.Max;
import java.util.List;

public interface IUserService {

    Result updateUser(User user);

    int deleteByPrimaryKey(Integer id);

    Result insert(User record, String code);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(User record);

    User selectByuserId(String userId);

    User selectByPhone(String phone);

    Result updatePassword(User user, String code);

    List<User> searchUser(String keyword);
}

