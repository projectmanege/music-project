package com.mangod.service.Impl;


import com.mangod.dao.FansMapper;
import com.mangod.entities.User;
import com.mangod.service.FansService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class FanServiceImpl  implements FansService {
    @Resource
    private FansMapper fansMapper;

    @Override
    public List<User> selectMyFans(String userId){
        List<User> users = fansMapper.selectMyFans(userId);
        return users;
    }

    @Override
    public List<User> selectMyFaned(String id) {
        List<User> users = fansMapper.selectMyFaned(id);
        return users;
    }

    @Override
    public int insert(String fansId,String userId) {
        fansMapper.insert(fansId,userId);
        return 1;
    }
    @Override
    public int deleteFaned(String fanedId,String userId){
            fansMapper.deleteFaned(fanedId,userId);
            return 1;
    }

    @Override
    public int countMyFans(String userId) {
        int num= fansMapper.countMyFans(userId);
        return num;
    }

    @Override
    public int countMyFaned(String userId) {
        int num= fansMapper.countMyFaned(userId);
        return num;
    }

    @Override
    public int IsFaned(String fanedId, String userId) {
        int num=fansMapper.IsFaned(fanedId,userId);
        return num;
    }

}
