package com.mangod.service.Impl;

import com.mangod.entities.User;
import com.mangod.lang.Result;
import com.mangod.service.IUserService;
import com.mangod.dao.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements IUserService {


    @Autowired
    UserMapper userMapper;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;
    private User user;

    @Override
    public Result updateUser(User user) {
        int res = userMapper.updateUser(user);
        if (res == 0) {
            return Result.fail(400, "修改失败", null);
        }
        User userResult = userMapper.selectByuserId(user.getUserId());
        return Result.succ(200, "修改成功", userResult);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        userMapper.deleteByPrimaryKey(id);
        return 1;
    }

    /**
     * 新增用户
     *
     * @param user
     * @param code
     * @return
     */
    @Override
    public Result insert(User user, String code) {
        if (code.equals(redisTemplate.opsForValue().get(user.getUserId()))) {
            user.setPhone(String.valueOf(user.getUserId()));
            userMapper.insert(user);
            return Result.succ(200, "注册成功", user);
        }
        return Result.fail(400, "验证码错误", null);
    }

    @Override
    public User selectByPrimaryKey(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(User record) {
        userMapper.updateByPrimaryKey(record);
        return 1;
    }

    @Override
    public User selectByuserId(String userId) {
        
        return userMapper.selectByuserId(userId);
    }

    @Override
    public User selectByPhone(String phone) {
        return userMapper.selectByPhone(phone);
    }

    @Override
    public Result updatePassword(User user, String code) {
        if (code.equals(redisTemplate.opsForValue().get(user.getUserId()))) {
            userMapper.updatePassword(user);
            return Result.succ(200, "修改成功", user);
        }
        return Result.fail(400, "验证码错误", null);
    }

    @Override
    public List<User> searchUser(String keyword) {
        return userMapper.searchUser(keyword);
    }
}
