package com.mangod.service;


import com.mangod.entities.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FansService  {

    int insert(@Param("userId") String userId, @Param("fansid") String fansid);

    List<User> selectMyFans(String userId);

    List<User> selectMyFaned(String id);

    int deleteFaned(@Param("fanedId") String fanedId, @Param("userId") String userId);

    int countMyFans(String userId);

    int countMyFaned(String userId);

    int IsFaned(@Param("fanedId") String fanedId, @Param("userId") String userId);

}
