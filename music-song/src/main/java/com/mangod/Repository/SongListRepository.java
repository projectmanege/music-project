package com.mangod.Repository;


import com.mangod.entities.SongSheet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
public class SongListRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public void insert(SongSheet songSheet) {
        mongoTemplate.save(songSheet);
        log.info("Mongo插入");
    }

    public void update(SongSheet songSheet) {
        Update update = new Update();
        update.set("songListName", songSheet.getSongListName());
        update.set("songSummary", songSheet.getSongSummary());
        mongoTemplate.updateMulti(new Query(Criteria.where("songListId").is(songSheet.getSongListId())), update, SongSheet.class);
        log.info("Mongo更新");
    }

    public List<SongSheet> findAll() {
        log.info("Mongo查询所有");
        return mongoTemplate.findAll(SongSheet.class);
    }

    public List<SongSheet> findByUserId(String userId) {
        log.info("Mongo查询");
        return mongoTemplate.find(new Query(Criteria.where("userId").is(userId)), SongSheet.class, "songSheet");
    }

    public List<SongSheet> findById(Integer id) {
        log.info("Mongo查询");
        return mongoTemplate.find(new Query(Criteria.where("songListId").is(id)), SongSheet.class, "songSheet");
    }

    public void delete(Integer id) {
        log.info("Mongo删除");
        mongoTemplate.remove(new Query(Criteria.where("songListId").is(id)),SongSheet.class,"songSheet");
    }

}
