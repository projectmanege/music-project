package com.mangod;

import com.mangod.Repository.SongListRepository;
import com.mangod.dao.SongListMapper;
import com.mangod.entities.SongSheet;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import java.util.List;

/**
 * UserMain8001
 *
 * @author qs
 * @date 2020/12/14 0014
 **/

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class SongMain8002 {
    public static void main(String[] args) {
        SpringApplication.run(SongMain8002.class, args);
    }

}
