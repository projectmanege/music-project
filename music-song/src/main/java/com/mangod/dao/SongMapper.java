package com.mangod.dao;

import com.mangod.entities.Song;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface SongMapper {


    int deleteFromSSL(@Param("songId") Integer songId,@Param("songSheetId")Integer songSheetId);

    int insert(Song record);

    int insertSelective(Song record);

    List<Song> selectAll();

    Song selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Song record);

    int updateByPrimaryKey(Song record);

    //根据名字查找歌曲
    List<Song> getSongByName(@Param("songName") String songName);

    //根据歌单id来搜索歌曲
    List<Song> selectSongsBySheet(@Param("sheetId") Integer sheetId);

    //判断SSL表中是否存在该歌曲
    Object checkExits(@Param("songSheetId") Integer songSheetId, @Param("songId") Integer songId);

    void insertSongToSongList(@Param("songSheetId") Integer songSheetId,@Param("songId") Integer songId);
}
