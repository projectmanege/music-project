package com.mangod.dao;

import com.mangod.entities.SongSheet;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SongListMapper {

    List<SongSheet> findAll();

    @Delete("delete from songList where id = #{id}")
    void deleteSongSheetById(Integer id);

    @Insert("insert into songList (id,userId,songListId,songListName,songSummary,imgs)values (#{id},#{userId},#{id},#{songListName},#{songSummary},#{imgs})")
    void insertSongSheet(SongSheet songSheet);

    @Update("update songList set userId = #{userId},songListId = #{songListId},songListName = #{songListName},songSummary = #{songSummary},imgs = #{imgs} where id = #{id}")
    void updateSongSheet(SongSheet songSheet);

    @Select("select * from songList where userId = #{userId}")
    List<SongSheet> findAllByuserId(String userId);

    @Select("select * from songList where id = #{songSheetId}")
    List<SongSheet> findBySongSheetId(Integer songSheetId);

    int deleteByPrimaryKey(Integer id);

    int insertSelective(SongSheet record);

    SongSheet selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SongSheet record);

    int updateByPrimaryKeyWithBLOBs(SongSheet record);

    int updateByPrimaryKey(SongSheet record);

}
