package com.mangod.service;


import com.mangod.entities.SongSheet;

import java.util.List;


public interface ISongSheetService {

    List<SongSheet> findAll();

    void insertSongSheet(SongSheet record);

    void deleteSongSheetById(Integer id);

    void updateSongSheet(SongSheet songSheet);

    List<SongSheet> findAllByuserId(String userId);

    List<SongSheet> findBySongSheetId(Integer songSheetId);
}
