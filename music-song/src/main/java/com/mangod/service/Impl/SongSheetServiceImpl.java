package com.mangod.service.Impl;

import com.mangod.Repository.SongListRepository;
import com.mangod.dao.SongListMapper;
import com.mangod.entities.SongSheet;
import com.mangod.service.ISongSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author beiye
 * @date 2020/12/15 0:33
 */
@Service
public class SongSheetServiceImpl implements ISongSheetService {

    @Autowired
    SongListMapper songListMapper;

    @Autowired
    SongListRepository songListRepository;

    @Override
    public List<SongSheet> findBySongSheetId(Integer songSheetId) {
//        List<SongSheet> songSheets = songListMapper.findBySongSheetId(songSheetId);
        List<SongSheet> songSheets = songListRepository.findById(songSheetId);
        return songSheets;
    }

    @Override
    public List<SongSheet> findAll() {
//        List<SongSheet> songSheets = songListMapper.findAll();
        List<SongSheet> songSheets = songListRepository.findAll();
        return songSheets;
    }

    @Override
    public void deleteSongSheetById(Integer id) {
        songListMapper.deleteSongSheetById(id);
        songListRepository.delete(id);
    }

    @Override
    public void insertSongSheet(SongSheet songSheet) {
        songSheet.setSongListId(songSheet.getId());
        songListMapper.insertSongSheet(songSheet);
        songListRepository.insert(songSheet);
    }

    @Override
    public void updateSongSheet(SongSheet songSheet) {
        songListMapper.updateSongSheet(songSheet);
        songListRepository.update(songSheet);
    }

    @Override
    public List<SongSheet> findAllByuserId(String userId) {
//        List<SongSheet> songSheets = songListMapper.findAllByuserId(userId);
        List<SongSheet> songSheets = songListRepository.findByUserId(userId);
        return songSheets;
    }
}
