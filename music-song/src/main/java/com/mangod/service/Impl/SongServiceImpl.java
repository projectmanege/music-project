package com.mangod.service.Impl;

import com.mangod.entities.Song;
import com.mangod.lang.Result;
import com.mangod.dao.SongMapper;
import com.mangod.service.ISongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @Author xuqy
 * @Date 2020/12/14 23:03
 * @Version 1.0
 */
@Service
public class SongServiceImpl implements ISongService {

    @Autowired
    SongMapper songMapper;

    /**
     * 获取歌曲列表
     * @return
     */
    @Override
    public List<Song> getAll() {
        return songMapper.selectAll();
    }

    /**
     * 根据歌名查找歌曲
     * @param songName
     * @return
     */
    @Override
    public List<Song> getSongByName(String songName) {
        return songMapper.getSongByName(songName);
    }

    /**
     * 根据id修改歌曲信息
     * @param song
     */
    @Override
    public void updateById(Song song) {
        songMapper.updateByPrimaryKey(song);
    }

    /**
     * 根据歌单id和歌曲id删除对应的联系
     * @param songId
     * @param songSheetId
     */
    @Override
    public void deleteSongById(Integer songId,Integer songSheetId) {
        songMapper.deleteFromSSL(songId,songSheetId);
    }

    /**
     * 插入新歌
     * @param song
     */
    @Override
    public void addSong(Song song) {
        songMapper.insert(song);
    }

    /**
     * 根据歌单搜索歌曲
     * @param sheetId
     */
    @Override
    public List<Song> selectSongsBySheetId(Integer sheetId) {
        List<Song> songs = songMapper.selectSongsBySheet(sheetId);
        return songs;
    }

    /**
     * 插入歌曲到指定的歌单中
     * @param songSheetId
     * @param songId
     * @return
     */
    @Override
    public Result addSongToSongList(Integer songSheetId, Integer songId) {
        Object result = songMapper.checkExits(songSheetId,songId);
        if (null==result){
            songMapper.insertSongToSongList(songSheetId,songId);
            return Result.succ(200,"收藏成功",null);
        }
        return Result.fail("歌曲已存在歌单中");
    }
}
