package com.mangod.service;


import com.mangod.entities.Song;
import com.mangod.lang.Result;

import java.util.List;

/**
 * @Author xuqy
 * @Date 2020/12/14 23:15
 * @Version 1.0
 */
public interface ISongService {

    List<Song> getAll();

    List<Song> getSongByName(String songName);

    void updateById(Song song);

    void deleteSongById(Integer songId,Integer songSheetId);

    void addSong(Song song);

    List<Song> selectSongsBySheetId(Integer sheetId);

    Result addSongToSongList(Integer songSheetId, Integer songId);

//    List<Song> getSongByName(String songName);

}
