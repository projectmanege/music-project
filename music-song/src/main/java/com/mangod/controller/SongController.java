package com.mangod.controller;

import com.mangod.entities.Song;
import com.mangod.lang.Result;
import com.mangod.service.ISongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

/**
 * @Author xuqy
 * @Date 2020/12/14 22:45
 * @Version 1.0
 */
@CrossOrigin
@RestController
@RequestMapping("/song")
public class SongController {

    @Autowired
    ISongService iSongService;

    /**
     * 获取歌曲列表
     * @return
     */
    @GetMapping("/getAll")
    public Result getAll(){
        List<Song> songList = iSongService.getAll();
        return Result.succ(songList);
    }

    /**
     * 添加歌曲
     * @param song
     * @return
     */
    @PostMapping("/addSong")
    public Result addSong(@RequestBody Song song){
        iSongService.addSong(song);
        return Result.succ(null);
    }

    /**
     * 根据名字查找歌曲
     * @param songName
     * @return
     */
    @GetMapping("/getSongByName/{songName}")
    public Result getSongByName(@PathVariable String songName){
        return Result.succ(iSongService.getSongByName(songName));
    }

    /**
     * 根据id修改歌曲的信息
     * @param song
     * @return
     */
    @PutMapping("/updateById")
    public Result updateById(@RequestBody Song song){
        iSongService.updateById(song);
        return Result.succ(null);
    }

    /**
     * 根据id删除歌曲
     * @param songId
     * @param songSheetId
     * @return
     */
    @DeleteMapping("/deleteSongById/{songId}/{songSheetId}")
    public void deleteSongById(@PathVariable Integer songId,@PathVariable Integer songSheetId){
        iSongService.deleteSongById(songId,songSheetId);
    }

    /**
     * 根据歌单id查找歌曲
     * @param sheetId
     * @return
     */
    @GetMapping("/selectSongsBySheetId/{sheetId}")
    public Result selectSongsBySheetId(@PathVariable Integer sheetId){
        List<Song> songs = iSongService.selectSongsBySheetId(sheetId);
        return Result.succ(songs);
    }

    /**
     * 收藏歌曲到歌单
     * @param songSheetId
     * @param songId
     * @return
     */
    @PostMapping("/addSongToSongList/{songSheetId}/{songId}")
    public Result addSongToSongList(@PathVariable Integer songSheetId,@PathVariable Integer songId){
        return iSongService.addSongToSongList(songSheetId,songId);

    }
}
