package com.mangod.controller;

import com.mangod.Repository.SongListRepository;
import com.mangod.dao.SongListMapper;

import com.mangod.entities.SongSheet;
import com.mangod.lang.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.mangod.service.ISongSheetService;

import java.util.List;

/**
 * @author beiye
 * @date 2020/12/15 0:34
 */
@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/songsheet")
public class SongSheetController {

    @Autowired
    ISongSheetService songSheetService;

    @Autowired
    SongListMapper songListMapper;

    @Autowired
    SongListRepository songListRepository;

    /**
     * 查询所有歌单
     * @return
     */
    @ResponseBody
    @GetMapping("/findAll")
    public Result findAll(){
        List<SongSheet> songSheets = songListMapper.findAll();
        for (SongSheet songSheet : songSheets) {
            songListRepository.insert(songSheet);
        }
        List<SongSheet> sheets = songSheetService.findAll();
//        log.info(String.valueOf(sheets));
        return Result.succ(sheets);
    }

    /**
     * 新增歌单
     * @param record
     * @return
     */
    @PostMapping("/insert")
    public Result insertSongSheet(@RequestBody SongSheet record) {
        songSheetService.insertSongSheet(record);
        return Result.succ(null);
    }

    /**
     * 根据id删除歌单
     * @param id
     * @return
     */
    @DeleteMapping("/deleteById/{id}")
    public void deleteSongSheetById(@PathVariable("id") Integer id){
        System.out.println("删除的歌单id："+id);
        songSheetService.deleteSongSheetById(id);

    }

    /**
     * 更新歌单信息
     * @param songSheet
     * @return
     */
    @PostMapping("/update")
    public Result updateSongSheet(@RequestBody SongSheet songSheet){
        songSheetService.updateSongSheet(songSheet);
        return Result.succ(null);
    }

    /**
     * 根据用户id查找歌单
     * @param userId
     * @return
     */
    @GetMapping("/findByuserId/{userId}")
    public Result findAllByuserId(@PathVariable String userId){
        List<SongSheet> songSheets = songSheetService.findAllByuserId(userId);
        System.out.println(songSheets.toString());
        return Result.succ(songSheets);
    }

    /**
     * 根据歌单id获取歌单信息
     * @param songSheetId
     * @return
     */
    @GetMapping("/findBySongSheetId/{songSheetId}")
    public Result findBySongSheetId(@PathVariable Integer songSheetId){
        List<SongSheet> songSheets = songSheetService.findBySongSheetId(songSheetId);
        return Result.succ(songSheets);
    }
}
