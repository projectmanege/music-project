package com.mangod;

import com.mangod.Repository.SongListRepository;
import com.mangod.dao.SongListMapper;
import com.mangod.entities.SongSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class Test {

    @Autowired
    SongListMapper songListMapper;

    @Autowired
    SongListRepository songListRepository;

    @org.junit.jupiter.api.Test
    public void test() {
        List<SongSheet> songSheets = songListMapper.findAll();
        for (SongSheet songSheet : songSheets) {
            songListRepository.insert(songSheet);
        }
    }
}
