package com.mangod.dao;


import com.mangod.entities.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {

    @Select("select * from user where userId = #{userId}")
    User selectByuserId(String userId);

    @Select("select * from user where userId = #{username}")
    User findByUsername(String username);
}