package com.mangod.config;

import com.mangod.component.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * SpringSecurity的配置
 * 通过SpringSecurity的配置，将JWTLoginFilter，JWTAuthenticationFilter组合在一起
 */


@Configuration
@EnableWebSecurity
public class MysecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


//    @Autowired
//    private MyUserDetailService myUserDetailService;
//
//    @Autowired
//    RedisPersistemRe ;

    @Resource
    private CustomizeAuthenticationSuccessHandler myAuthenticationSuccessHandler;
    @Resource
    private CustomizeAuthenctiationFailureHandler myAuthenctiationFailureHandler;
    @Resource
    private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;
    @Resource
    private CustomizeLogoutSuccessHandler logoutSuccessHandler;

//    private PasswordEncoder passwordEncoder;
//
//    private CustomizeUserDetailsService userDetailsService;

//    @Autowired
//    RememberUserDetailService rememberUserDetailService;


//    public MysecurityConfig(CustomizeUserDetailsService userDetailsService,PasswordEncoder passwordEncoder) {
//        this.userDetailsService = userDetailsService;
//        this.passwordEncoder = passwordEncoder;
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //super.configure(http);
        //定制请求的授权规则
        http
                .authorizeRequests()
                .antMatchers("/user/sendCode","/user/insert","/user/sendCodeUpdatePass","/user/updatePassword").anonymous()
                .antMatchers("/user/sendCode","/user/insert","/user/sendCodeUpdatePass","/user/updatePassword").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTLoginFilter(authenticationManager()))
                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
                .cors()
                .and().csrf().disable();

        http.formLogin().permitAll()
                .loginProcessingUrl("/login")   //登录接口(POST)
                .successHandler(myAuthenticationSuccessHandler) //注册自定义处理器
                .failureHandler(myAuthenctiationFailureHandler);
//                .loginPage("http://localhost:8080/")    //登录页(GET)


        http.exceptionHandling()
                .authenticationEntryPoint(customAuthenticationEntryPoint);//匿名用户访问无权限资源时的异常
//                .accessDeniedHandler();   //用来解决认证过的用户访问无权限资源时的异常

        http.logout().
                permitAll().//允许所有用户
                logoutSuccessHandler(logoutSuccessHandler).//登出成功处理逻辑
                deleteCookies("Authorization");

    }

//    @Override
//    public void configure(WebSecurity web) {
//        //解决静态资源被拦截的问题
//        web.ignoring().antMatchers("/user/sendCode");
//    }

//    /**
//     * 重新实例化bean
//     */
//    @Override
//    @Bean
//    public AuthenticationManager authenticationManagerBean() throws Exception {
//        return super.authenticationManagerBean();
//    }
//
//
//        @Override
//    public void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
//    }


}
