package com.mangod.component;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mangod.lang.JsonResult;
import com.mangod.utils.ResultTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * CustomizeAuthenticationSuccessHandler
 *
 * @author qs
 * @date 2020/12/17 0017
 **/
@Component
public class CustomizeAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
//        Result result = new Result();
//        result.setCode(200);
//        result.setMsg("登录成功！");
//        result.setState(true);
//        Integer userId = Integer.valueOf(SecurityContextHolder.getContext().getAuthentication().getName());
//        User user = userService.selectByuserId(userId);
//        result.setData(user);//响应数据携带用户名
//        // 把result对象转成 json 格式 字符串 通过 response 以application/json;charset=UTF-8 格式写到响应里面去
//        httpServletResponse.setContentType("application/json; charset=utf-8");
//        PrintWriter out = httpServletResponse.getWriter();
//        out.write(JSON.toJSONString(result));

        //返回json数据
        JsonResult result = ResultTool.success();
        //处理编码方式，防止中文乱码的情况
        httpServletResponse.setContentType("text/json;charset=utf-8");
        //塞到HttpServletResponse中返回给前台
        httpServletResponse.getWriter().write(JSON.toJSONString(result));
    }
}
