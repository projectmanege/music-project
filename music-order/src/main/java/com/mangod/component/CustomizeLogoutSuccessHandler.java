package com.mangod.component;

import com.alibaba.fastjson.JSON;
import com.mangod.lang.JsonResult;
import com.mangod.utils.ResultTool;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * CustomizeLogoutSuccessHandler
 *
 * @author qs
 * @date 2020/12/17 0017
 **/
@Component
public class CustomizeLogoutSuccessHandler implements LogoutSuccessHandler {
    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
//        Result result=new Result();
//        httpServletResponse.setContentType("application/json; charset=utf-8");
//        result.setCode(1000);
//        result.setMsg("注销成功！");
//        result.setState(true);
//        PrintWriter out = httpServletResponse.getWriter();
//        out.write(JSON.toJSONString(result));
//        httpServletResponse.getWriter().flush();

        JsonResult result = ResultTool.success();
        httpServletResponse.setContentType("text/json;charset=utf-8");
        httpServletResponse.getWriter().write(JSON.toJSONString(result));
    }
}
