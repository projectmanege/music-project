package com.mangod.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mangod.entities.User;
import com.mangod.lang.MouldeNameURL;
import com.mangod.lang.Result;
import com.mangod.service.IUserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
/**
 * JWTLoginFilter
 *
 * @author qs
 * @date 2020/12/17 0017
 **/

public class JWTLoginFilter extends UsernamePasswordAuthenticationFilter {
    @Autowired
    IUserService userService;
    @Autowired
    RestTemplate restTemplate;
    private AuthenticationManager authenticationManager;
    public JWTLoginFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }
    // 接收并解析用户凭证
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            User user = new ObjectMapper()
                    .readValue(req.getInputStream(), User.class);
            logger.info(user);
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            user.getUserId(),
                            user.getPassword(),
                            new ArrayList<>())
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    // 用户成功登录后，这个方法会被调用，我们在这个方法里生成token
    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
       String token = Jwts.builder()
                .setSubject(((org.springframework.security.core.userdetails.User) auth.getPrincipal()).getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + 60 * 60 * 24 * 1000))
                .signWith(SignatureAlgorithm.HS512, "MyJwtSecret")
                .compact();
        logger.info(token);
//        手动注入userService
        BeanFactory factory = WebApplicationContextUtils.getRequiredWebApplicationContext(req.getServletContext());
        userService = (IUserService) factory.getBean("userServiceImpl");
//
        String userId = ((org.springframework.security.core.userdetails.User)auth.getPrincipal()).getUsername();
        User user = userService.selectByuserId(userId);
//        com.mangod.entities.User user = restTemplate.getForObject(MouldeNameURL.USERMODULEURL + "/user/findByuserId/" + userId, com.mangod.entities.User.class);
        Result result = Result.succ(200,"登录成功~",user);
        res.setContentType("application/json;charset=UTF-8");
        res.addHeader("Authorization", "Bearer " + token);
        res.addHeader("Access-Control-Expose-Headers","Authorization");
        PrintWriter out = res.getWriter();
        out.write(new ObjectMapper().writeValueAsString(result));
        out.flush();
        out.close();
    }
}
