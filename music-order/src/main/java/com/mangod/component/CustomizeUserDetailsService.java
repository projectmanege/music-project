package com.mangod.component;


import com.mangod.lang.MouldeNameURL;
import com.mangod.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * CustomizeUserDetailsService
 *
 * @author qs
 * @date 2020/12/17 0017
 **/
@Configuration
public class CustomizeUserDetailsService implements UserDetailsService {

    @Autowired
    IUserService userService;

//    @Autowired
//    private Encryption encryption;

//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.mangod.entities.User user = userService.selectByuserId(username);
        Collection<GrantedAuthority> authorities = authorities();
        if(username==null||username.equals("")){
            throw new RuntimeException("用户名不能为空！");
        }
        if(user==null){
            throw new RuntimeException("用户名不存在！");
        }
//        System.out.println(passwordEncoder.encode(user.getPassword()));
        return new User(String.valueOf(user.getUserId()), user.getPassword(), authorities);
    }

    private Collection<GrantedAuthority> authorities() {
        List<GrantedAuthority> authorityList = new ArrayList<GrantedAuthority>();
        authorityList.add(new SimpleGrantedAuthority("USER"));
        return authorityList;
    }
}
