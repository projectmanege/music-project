package com.mangod.component;

import com.alibaba.fastjson.JSON;
import com.mangod.lang.JsonResult;
import com.mangod.lang.ResultCode;
import com.mangod.utils.ResultTool;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * CustomizeAuthenctiationFailureHandler
 *
 * @author qs
 * @date 2020/12/17 0017
 **/
@Component
public class CustomizeAuthenctiationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        //super.onAuthenticationFailure(request, response, exception);
//        Result result=new Result();
//        if(exception instanceof BadCredentialsException){
//            //密码错误
//            result.setCode(1200);
//            result.setMsg("密码错误！");
//        }else if(exception instanceof InternalAuthenticationServiceException){
//            //用户不存在
//            result.setCode(1100);
//            result.setMsg("不存在该用户！");
//        }
//        else if(exception instanceof AccountExpiredException){
//            //账号过期
//        }
//        else if(exception instanceof CredentialsExpiredException){
//            //密码过期
//        }else if(exception instanceof DisabledException){
//            //帐号不可用
//        }else if(exception instanceof LockedException){
//            //帐号锁定
//        }else{
//            //其他错误
//        }
//        result.setState(false);
//        // 把result对象转成 json 格式 字符串 通过 response 以application/json;charset=UTF-8 格式写到响应里面去
//        response.setContentType("application/json; charset=utf-8");
//        PrintWriter out = response.getWriter();
//        out.write(JSON.toJSONString(result));

        //返回json数据
        JsonResult result = null;
        if (e instanceof AccountExpiredException) {
            //账号过期
            result = ResultTool.fail(ResultCode.USER_ACCOUNT_EXPIRED);
        } else if (e instanceof BadCredentialsException) {
            //密码错误
            result = ResultTool.fail(ResultCode.USER_CREDENTIALS_ERROR);
        } else if (e instanceof CredentialsExpiredException) {
            //密码过期
            result = ResultTool.fail(ResultCode.USER_CREDENTIALS_EXPIRED);
        } else if (e instanceof DisabledException) {
            //账号不可用
            result = ResultTool.fail(ResultCode.USER_ACCOUNT_DISABLE);
        } else if (e instanceof LockedException) {
            //账号锁定
            result = ResultTool.fail(ResultCode.USER_ACCOUNT_LOCKED);
        } else if (e instanceof InternalAuthenticationServiceException) {
            //用户不存在
            result = ResultTool.fail(ResultCode.USER_ACCOUNT_NOT_EXIST);
        }else{
            //其他错误
            result = ResultTool.fail(ResultCode.COMMON_FAIL);
        }
        //处理编码方式，防止中文乱码的情况
        response.setContentType("text/json;charset=utf-8");
        //塞到HttpServletResponse中返回给前台
        response.getWriter().write(JSON.toJSONString(result));

    }
}
