package com.mangod.service.Impl;

import com.mangod.dao.UserMapper;
import com.mangod.entities.User;
import com.mangod.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public User selectByuserId(String userId) {
        return userMapper.selectByuserId(userId);
    }
}
