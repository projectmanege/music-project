package com.mangod.service;

import com.mangod.entities.User;

public interface IUserService {

    User selectByuserId(String userId);
}

