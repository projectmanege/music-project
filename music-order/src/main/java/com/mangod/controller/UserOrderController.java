package com.mangod.controller;

import com.alibaba.fastjson.JSONObject;
import com.mangod.entities.SignUp;
import com.mangod.entities.User;
import com.mangod.lang.MouldeNameURL;
import com.mangod.lang.Result;
import com.mangod.utils.UploadFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;
import java.util.UUID;

/**
 * UserOrderController
 *
 * @author qs
 * @date 2020/12/18 0018
 **/
@CrossOrigin
@Slf4j
@RestController
@RequestMapping("user")
public class UserOrderController {

    @Resource
    private RestTemplate restTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 查询所有用户
     * @return
     */
    @GetMapping("/findAll")
    public Result findAll(){
        return restTemplate.getForObject(MouldeNameURL.SONGMODULEURL + "/songsheet/findAll", Result.class);

    }

    @PostMapping("/sendCode")
    public Result sendCode(@RequestBody String signUp){
        return restTemplate.postForObject(MouldeNameURL.USERMODULEURL + "/user/sendCode/", signUp, Result.class);

    }

    @PostMapping("/sendCodeUpdatePass")
    public Result sendCodeUpdatePass(@RequestBody String signUp){
        return restTemplate.postForObject(MouldeNameURL.USERMODULEURL + "/user/sendCodeUpdatePass/", signUp, Result.class);

    }

    @PostMapping("/insert")
    public Result insert(@RequestBody SignUp signUp){
        signUp.setPass(passwordEncoder.encode(signUp.getPass()));
        return restTemplate.postForObject(MouldeNameURL.USERMODULEURL + "/user/insert" , signUp, Result.class);

    }

    @PutMapping("/updatePassword")
    public Result updatePass(@RequestBody SignUp signUp){
        signUp.setPass(passwordEncoder.encode(signUp.getPass()));
        return restTemplate.postForObject(MouldeNameURL.USERMODULEURL + "/user/updatePassword" , signUp, Result.class);

    }


    @PostMapping("/upload")
    public Result uploadFile(@RequestParam MultipartFile file){
        String fileName = "..\\" + UploadFile.uploadFile(file, "C:\\Users\\Administrator\\IdeaProjects\\music\\public\\");
        return Result.succ(200,"更换成功",fileName);
//        return restTemplate.postForObject(MouldeNameURL.USERMODULEURL + "/user/upload",file, Result.class);
    }

    @PutMapping("/update")
    public Result updateUser(@RequestBody User user){

        return restTemplate.postForObject(MouldeNameURL.USERMODULEURL + "/user/update",user, Result.class);
    }

//    @PostMapping("/findAll")
//    public Result findAll(){
//        return restTemplate.getForObject(MouldeNameURL.SONGMODULEURL + "/songsheet/findAll", Result.class);
//
//    }

    @GetMapping("/selectByuserId/{userId}")
    public Result findByUserId(@PathVariable String userId) {
        return restTemplate.getForObject(MouldeNameURL.USERMODULEURL + "/user/selectByuserId/" + userId, Result.class);

    }

    @GetMapping("/findByuserId/{userId}")
    public Result selectByPrimaryKey(@PathVariable String userId){
        return restTemplate.getForObject(MouldeNameURL.USERMODULEURL + "/user/selectByuserId/" + userId, Result.class);


    }

    @GetMapping("/searchUser/{keyword}")
    public Result searchUser(@PathVariable String keyword) {
        return restTemplate.getForObject(MouldeNameURL.USERMODULEURL + "/user/searchUser/" + keyword, Result.class);
    }
}
