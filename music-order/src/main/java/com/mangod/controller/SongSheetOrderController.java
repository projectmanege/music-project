package com.mangod.controller;

import com.mangod.entities.SongSheet;
import com.mangod.lang.MouldeNameURL;
import com.mangod.lang.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * SongSheetOrderController
 *
 * @author qs
 * @date 2020/12/18 0018
 **/
@CrossOrigin
@Slf4j
@RestController
@RequestMapping("songsheet")
public class SongSheetOrderController {
    @Resource
    private RestTemplate restTemplate;

    /**
     * 查询所有歌单
     * @return
     */
    @GetMapping("/findAll")
    public Result findAll(){
        return restTemplate.getForObject(MouldeNameURL.SONGMODULEURL + "/songsheet/findAll", Result.class);

    }

    /**
     * 新增歌单
     * @param record
     * @return
     */
    @PostMapping("/insert")
    public Result insertSongSheet(@RequestBody SongSheet record) {
        return restTemplate.postForObject(MouldeNameURL.SONGMODULEURL + "/songsheet/insert", record, Result.class);

    }

    /**
     * 根据id删除歌单
     * @param id
     * @return
     */
    @DeleteMapping("/deleteById/{id}")
    public Result deleteSongSheetById(@PathVariable("id") Integer id){
        System.out.println("MouldeNameURL:"+id);
        restTemplate.delete(MouldeNameURL.SONGMODULEURL + "/songsheet/deleteById/" + id);
        return Result.succ("操作成功");
    }

    /**
     * 更新歌单信息
     * @param songSheet
     * @return
     */
    @PutMapping("/update")
    public Result updateSongSheet(@RequestBody SongSheet songSheet){
        return restTemplate.postForObject(MouldeNameURL.SONGMODULEURL + "/songsheet/update", songSheet, Result.class);
    }

    /**
     * 根据用户id查找歌单
     * @param userId
     * @return
     */
    @GetMapping("/findByuserId/{userId}")
    public Result findAllByuserId(@PathVariable String userId){
        return restTemplate.getForObject(MouldeNameURL.SONGMODULEURL + "/songsheet/findByuserId/" + userId, Result.class);

    }

    /**
     * 根据歌单id获取歌单信息
     * @param songSheetId
     * @return
     */
    @GetMapping("/findBySongSheetId/{songSheetId}")
    public Result findBySongSheetId(@PathVariable Integer songSheetId){
        return restTemplate.getForObject(MouldeNameURL.SONGMODULEURL + "/songsheet/findBySongSheetId/" + songSheetId, Result.class);
    }

}
