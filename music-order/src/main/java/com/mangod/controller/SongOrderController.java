package com.mangod.controller;

import com.mangod.entities.Song;
import com.mangod.lang.MouldeNameURL;
import com.mangod.lang.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * SongOrderController
 *
 * @author qs
 * @date 2020/12/18 0018
 **/
@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/song")
public class SongOrderController {


    @Resource
    private RestTemplate restTemplate;

    public static final String PAYMENT_URL = "http://localhost:8002";


    /**
     * 获取歌曲列表
     * @return
     */
    @GetMapping("/getAll")
    public Result getAll(){
        return restTemplate.getForObject(MouldeNameURL.SONGMODULEURL + "/song/getAll", Result.class);

    }

    /**
     * 添加歌曲
     * @param song
     * @return
     */
    @PostMapping("/addSong")
    public Result addSong(@RequestBody Song song){
        return restTemplate.postForObject(MouldeNameURL.SONGMODULEURL + "/song/addSong",song, Result.class);

    }

    /**
     * 根据名字查找歌曲
     * @param songName
     * @return
     */
    @GetMapping("/getSongByName/{songName}")
    public Result getSongByName(@PathVariable String songName){
        return restTemplate.getForObject(MouldeNameURL.SONGMODULEURL + "/song/getSongByName/" + songName, Result.class);
    }

    /**
     * 根据id修改歌曲的信息
     * @param song
     * @return
     */
    @PutMapping("/updateById")
    public Result updateById(@RequestBody Song song){
        return restTemplate.postForObject(MouldeNameURL.SONGMODULEURL + "/song/updateById", song, Result.class);
    }

    /**
     * 根据id删除歌曲
     * @param songId
     * @param songSheetId
     * @return
     */
    @DeleteMapping("/deleteSongById/{songId}/{songSheetId}")
    public Result deleteSongById(@PathVariable Integer songId,@PathVariable Integer songSheetId){
        restTemplate.delete(MouldeNameURL.SONGMODULEURL + "/song/deleteSongById/" + songId + "/" + songSheetId, Result.class);
        return Result.succ("操作成功");
    }

    /**
     * 根据歌单id查找歌曲
     * @param sheetId
     * @return
     */
    @GetMapping("/selectSongsBySheetId/{sheetId}")
    public Result selectSongsBySheetId(@PathVariable Integer sheetId){
        return restTemplate.getForObject(PAYMENT_URL + "/song/selectSongsBySheetId/" + sheetId, Result.class);

    }

    /**
     * 收藏歌曲到歌单
     * @param songSheetId
     * @param songId
     * @return
     */
    @PostMapping("/addSongToSongList/{songSheetId}/{songId}")
    public Result addSongToSongList(@PathVariable Integer songSheetId,@PathVariable Integer songId){
        return restTemplate.postForObject(MouldeNameURL.SONGMODULEURL + "/song/addSongToSongList/" + songSheetId + "/" + songId, null, Result.class);

    }
}
