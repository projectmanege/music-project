package com.mangod.controller;

import com.mangod.entities.User;
import com.mangod.lang.MouldeNameURL;
import com.mangod.lang.Result;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

/**
 * FansOrderController
 *
 * @author qs
 * @date 2020/12/22 0022
 **/
@RestController
public class FansOrderController {

    @Resource
    private RestTemplate restTemplate;


    @GetMapping("/IsFaned/{userId}/{fandId}")
    public Result IsFaned(@PathVariable String fandId, @PathVariable String userId){
        return restTemplate.getForObject(MouldeNameURL.USERMODULEURL + "/IsFaned/" + userId + "/" + fandId, Result.class);

    }

    @GetMapping("/getFansByUserId/{userId}")
    public Result getFansByUserId(@PathVariable String userId){
        return restTemplate.getForObject(MouldeNameURL.USERMODULEURL + "/getFansByUserId/" + userId, Result.class);

    }
    @GetMapping("/getFanedByuserId/{userId}")
    public Result getFanedByuserId(@PathVariable String userId){
        return restTemplate.getForObject(MouldeNameURL.USERMODULEURL + "/getFanedByuserId/" + userId, Result.class);

    }

    /**
     * 取消关注
     * @param fandId 关注的人的id
     * @param userId 当前用户id
     * @return
     */
    @DeleteMapping("/deleteFanedByuserId/{fandId}/{userId}")
    public Result deleteFanedByuserId(@PathVariable String fandId,@PathVariable String userId){
         restTemplate.delete(MouldeNameURL.USERMODULEURL + "/deleteFanedByuserId/" + fandId + "/" + userId);
         return Result.succ(null);
    }

    /**
     * 关注
     * @param userId 要关注的人
     * @param fansId 当前登录的用户
     * @return
     */
    @PostMapping("/addFans/{userId}/{fansId}")
    public  Result insert(@PathVariable String userId,@PathVariable String fansId){
        restTemplate.postForObject(MouldeNameURL.USERMODULEURL + "/addFans/" + userId + "/" + fansId, null, Result.class);
        return Result.succ(null);
    }
    @GetMapping("/countMyFans/{userId}")
    public Result countMyFans(@PathVariable String userId){
        return restTemplate.getForObject(MouldeNameURL.USERMODULEURL + "/countMyFans/" + userId, Result.class);

    }
    @GetMapping("/countMyFaned/{userId}")
    public Result countMyFaned(@PathVariable String userId){
        return restTemplate.getForObject(MouldeNameURL.USERMODULEURL + "/countMyFaned/" + userId, Result.class);
    }

}
