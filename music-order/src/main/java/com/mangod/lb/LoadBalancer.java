package com.mangod.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * LoadBalancer
 *
 * @author qs
 * @date 2020/11/18 0018
 **/
public interface LoadBalancer {

    ServiceInstance instanes(List<ServiceInstance> serviceInstanceList);
}
